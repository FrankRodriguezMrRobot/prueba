// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: "AIzaSyANgGvdxYMyHY4x8QRi1V1c1ynnwN1VN7Q",
  authDomain: "ingenium-2bee1.firebaseapp.com",
  projectId: "ingenium-2bee1",
  storageBucket: "ingenium-2bee1.appspot.com",
  messagingSenderId: "284745327463",
  appId: "1:284745327463:web:dacce78bc44e37a6e385c2"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
